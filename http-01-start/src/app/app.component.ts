import { PostsService } from './posts.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Post } from './post.model';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  loadedPosts : Post[] = [];
  isFetching = false;
  error!:HttpErrorResponse;
  private subError:Subscription;

  constructor(private http: HttpClient, private postsService : PostsService) {}

  ngOnInit() {
   this.fetchPosts();

   this.subError = this.postsService.error.subscribe( err => {
      this.error = err;
    })



  }

  onCreatePost(postData: Post) {
    this.postsService.createAndStorePost(postData.title, postData.content)
  }

  onFetchPosts() {
    // Send Http request
    this.fetchPosts();
  }

  onClearPosts() {
    // Send Http request
    this.postsService.clearPosts().subscribe( ret => {
      this.loadedPosts = [];
    })
  }

  private fetchPosts() {
    this.isFetching = true;
    this.postsService.fetchPosts().subscribe( posts => {
      this.isFetching = false;
      this.loadedPosts = posts;
    }, error=>{
      this.isFetching = false;
      this.error = error;

    });

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subError.unsubscribe();

  }






}
