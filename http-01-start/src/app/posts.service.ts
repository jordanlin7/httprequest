import { HttpClient, HttpErrorResponse, HttpEventType, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Post } from './post.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  error = new Subject<HttpErrorResponse>();



  constructor(private http: HttpClient) { }

  createAndStorePost(title:string, content: string) {
    // Send Http request
    const postData: Post = {title:title, content:content}

    // post會回傳一個observable，如果沒人訂閱的話，那這個request就會被當成無效的
    this.http.post<{name : string}>(
      'https://courseproject-shopping-default-rtdb.firebaseio.com/posts.json',       // url, firebaser所提供的database 網址 + posts.json(firebase會建一個類似的資料夾叫posts)
      postData,
      {
        observe: 'response'
      }                                                                      // 傳送的資料，通常是JSON data，在這邊angular httpclient會自動幫我們把javascript object轉成JSON
    ).subscribe(ret => {
      console.log(ret)
    },
    error => {
      this.error.next(error);
    })


  }

  fetchPosts() {
    let searchParams = new HttpParams();
    searchParams = searchParams.set('print', 'pretty')
    searchParams = searchParams.set('test', 'content')



    return this.http.get<{[key : string] : Post}>(
      'https://courseproject-shopping-default-rtdb.firebaseio.com/posts.json',
      {
        headers : new HttpHeaders({'Custom-Header' : 'Hello' }),
        params: searchParams

      }
    ).pipe(
      map( responseData => {
        const postsArray : Post[] = [];
        for(const key in responseData) {
          if(responseData.hasOwnProperty(key)) {
            postsArray.push({...responseData[key], id : key})
          }
        }
        return postsArray;
      }),
      catchError(errorRes => {
        return throwError(errorRes)

      })
    )
  }

  clearPosts() {
    return this.http.delete(
      'https://courseproject-shopping-default-rtdb.firebaseio.com/posts.json',
      {
        observe : 'events'

      }
    ).pipe(
      tap(event=> {
        console.log(event)
        if(event.type === HttpEventType.Sent) {
          // sending...
        }
        if(event.type === HttpEventType.Response) {
          console.log(event.body);
        }

      })
    );
  }





}
